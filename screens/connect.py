import requests
import json
import base64


def create_screenshot(target_url):
    url = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + target_url + '&screenshot=true&key=AIzaSyC9-79xyRpCrLLReQ4sGn28m50twIEBCRE'
    response = requests.get(url)
    response_json = json.loads(response.text)
    audits = response_json['lighthouseResult']['audits']
    encoded_screenshot = audits['final-screenshot']['details']['data']
    imgdata = base64.b64decode(encoded_screenshot.replace(' ', '+').replace('data:image/jpeg;base64,', ''))

    with open('test_image.jpg', 'wb') as f:
        f.write(imgdata)

    return imgdata
