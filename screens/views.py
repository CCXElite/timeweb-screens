from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from . import connect


class AddTaskView(APIView):
    def post(self, request):
        target_url = request.POST['target_url']
        print(target_url)
        screen_data = connect.create_screenshot(target_url)
        return Response(status=201, data=screen_data)
